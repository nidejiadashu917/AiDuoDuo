import requests
import json
from aip import AipSpeech
import pyaudio
import wave
import os
from pydub import AudioSegment
import time
from pydub.playback import play

# 设置百度语音识别API的信息(可以去百度申请,有个尝鲜自己玩目前够，有能力的大神自己改)
APP_ID = 'xxxxxxxx'
API_KEY = 'O3drxxxxxxxxxxxxxxBh'
SECRET_KEY = 'S2xxxxxxxxxxxxxxxxxxxxxxxxav'
client = AipSpeech(APP_ID, API_KEY, SECRET_KEY)

# 设置百度语音合成API的信息(可以去百度申请,有个尝鲜自己玩目前够，有能力的大神自己改)
APP_ID_TTS = 'xxxxxxxx'
API_KEY_TTS = 'O3drxxxxxxxxxxxxxxBh'
SECRET_KEY_TTS = 'S2xxxxxxxxxxxxxxxxxxxxxxxxav'

url = "http://api.aixiaoxing.com/api/v2/cn8196"

headers = {
    "Content-Type": "application/json",
    "X-API-Key": "cn8196-1X4XX2XXX4XXXX15X55",  # 开发者APIKey号码(可以发邮件去申请,好像申请后额度是500次调用.没有查的后台我也不知道.自己去官网问吧)
    "X-User-ID": "50029"  # 用户的独立ID号码(这个可以随便写，看官方文档应该是用户的独立ID)
}

# 配置录音参数
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
CHUNK = 1024
RECORD_SECONDS = 5
WAVE_OUTPUT_FILENAME = "output.wav"

# 初始化录音器
audio = pyaudio.PyAudio()

# 定义录音函数
def record():
    # 打开录音器
    input_device_index = 1  # 麦克风设备的ID
    stream = audio.open(format=FORMAT, channels=CHANNELS, rate=RATE,
                        input=True, input_device_index=input_device_index,
                        frames_per_buffer=CHUNK)

    print("请说话...")

    frames = []

    # 录制音频，增加超时时间
    timeout = time.time() + RECORD_SECONDS
    while time.time() < timeout:
        data = stream.read(CHUNK)
        frames.append(data)

    # 关闭录音器
    stream.stop_stream()
    stream.close()
    audio.terminate()

    # 保存录制的音频
    wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(audio.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(frames))
    wf.close()


# 定义语音识别函数
def recognize():
    with open(WAVE_OUTPUT_FILENAME, 'rb') as f:
        audio_data = f.read()
        result = client.asr(audio_data, 'wav', RATE, {
            'dev_pid': 1537,
        })
        if result['err_no'] == 0:
            return result['result'][0]
        else:
            return None

# 定义语音合成函数
def speak(text):

    try:
        result = client.synthesis(text, 'zh', 1, {
            'vol': 7,
            'spd': 6,
            'per': 5118,
        })

        if not isinstance(result, dict):
            with open('output.mp3', 'wb') as f:
                f.write(result)

            # 播放声音
            sound = AudioSegment.from_mp3("output.mp3")
            play(sound)
    except Exception as e:
        print("语音合成出错：", e)
    finally:
        # 删除音频文件
        os.remove(WAVE_OUTPUT_FILENAME)
        os.remove("output.mp3")

count = 1
while count < 100:  # 循环10次
    try:
        record()
        question = recognize()
        if question:
            count += 1
            data = {
                "question": question, # 用户提问信息
                "role": "02003",  # 开发者设置机器人角色，可以指定角色(这个官方文档有公开部分模型的角色，我选择的是AI女仆朵朵)
                "access_internet": True,
                "access_result": 3,
                "messages": []
            }
            response = requests.post(url, json=data, headers=headers)
            response_json = response.json()
            answer = response_json.get("answer")

            print("bot：", answer)
            speak(answer)  # 输出可爱女孩的声音
            
    except KeyboardInterrupt:
        print("程序已手动停止。")
        break
    except Exception as e:
        print(e)
